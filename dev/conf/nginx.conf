user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
  worker_connections  1024;
}


http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;

  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';

  access_log  /var/log/nginx/access.log  main;

  sendfile           on;
  keepalive_timeout  65;

  gzip            on;
  gzip_comp_level 5;
  gzip_min_length 256;
  gzip_proxied    any;
  gzip_vary       on;
  gzip_types
    application/atom+xml
    application/javascript
    application/json
    application/ld+json
    application/manifest+json
    application/rss+xml
    application/vnd.geo+json
    application/vnd.ms-fontobject
    application/x-font-ttf
    application/x-web-app-manifest+json
    application/xhtml+xml
    application/xml
    font/opentype
    image/bmp
    image/svg+xml
    image/x-icon
    text/cache-manifest
    text/css
    text/plain
    text/vcard
    text/vnd.rim.location.xloc
    text/vtt
    text/x-component
    text/x-cross-domain-policy;

  resolver 127.0.0.11;

  map $is_args $and_args {
    default '';
    '?' '&';
  }

  server {
    listen       8080;
    server_name  localhost;

    # Stop the server from redirecting to a different port
    absolute_redirect off;

    # Default document root is the /srv/webroot folder.
    root /srv/webroot;

    ##
    #  Security configuration
    ##

    # Don't log requests to favicon.ico.
    location = /favicon.ico {
      log_not_found off;
      access_log off;
    }

    # Don't log requests to robots.txt.
    location = /robots.txt {
      log_not_found off;
      access_log off;
    }

    # Deny access to all hidden files
    location ~ /\. {
      deny all;
    }

    location = /auth-image {
      internal;
      set $id '';
      if ($request_uri ~ ^/dl/[^/]+/P0*(\d+)) {
          set $id $1;
      }
      proxy_pass http://127.0.0.1:8080/artifacts/auth-image/$id;
    }

    ##
    # ROOT ROUTES
    ##

    # Try to serve file or directory index from /srv/webroot before
    # falling back to @cake.
    location / {
      try_files $uri @cake;

      # Set auth for images
      location /dl {
        auth_request /auth-image;
        try_files $uri @image-proxy;
        error_page 401 403 = @unauthorized;
      }
    }


  # Ensure static webroot assets don't bother 404-ing in Drupal.
  location ~* \.(css|eps|gif|gz|html?|js|jpe?g|ico|pdf|png|tiff?|txt|zip)$ {
    try_files $uri @cake;
  }
    location @unauthorized {
      return 404;
    }

    location @image-proxy {
      return 302 https://cdli.ucla.edu$request_uri;
    }

    # Named location for rewrites directly to Cake.
    location @cake {
      root /srv/app/cake;
      include fastcgi_params;
      fastcgi_param SCRIPT_FILENAME $document_root/index.php;
      fastcgi_param SCRIPT_NAME /index.php;
      set $upstream app_cake:9000;
      fastcgi_pass  $upstream;

      #fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
  }
}
