const { util } = require('@citation-js/core')
const { parse: parseDate } = require('@citation-js/date')
const { parse: parseName } = require('@citation-js/name')

function createCdliNumber (id) {
    return 'P' + id.toString().padStart(6, '0')
}

function parseCdliAuthor ({ author: { id, author } }) {
    if (id === 820) {
        return { literal: 'Cuneiform Digital Library Iniative (CDLI)' }
    } else {
        return parseName(author)
    }
}

module.exports = new util.Translator([
    { source: null, target: 'type', convert: { toTarget () { return 'webpage' } } },
    {
        source: 'designation',
        target: 'title',
        convert: { toTarget (designation) { return `${designation} artifact entry` } }
    },
    {
        source: 'artifacts_updates',
        target: ['author', 'issued'],
        convert: {
            toTarget (updates) {
                const updateEvent = updates
                    .map(update => update.update_event)
                    .filter(Boolean)
                    .sort((a, b) => a.created < b.created)
                    .pop()

                const authors = updateEvent.authors
                    .filter(({ id }, index) =>
                        updateEvent.authors.findIndex(author => author.id === id) === index)
                    .map(parseCdliAuthor)

                const data = parseDate(updateEvent.created.split('T')[0])

                return [authors, date]
            }
        }
    },
    {
        source: 'modified',
        target: 'issued',
        convert: { toTarget: parseDate }
    },
    {
        source: null,
        target: 'accessed',
        convert: {
            toTarget () { return parseDate(Date.now()) }
        }
    },
    {
        source: 'inscriptions',
        target: 'abstract',
        convert: {
            toTarget ([inscription]) {
                return inscription ? inscription.transliteration_clean : undefined
            }
        }
    },
    {
        source: 'id',
        target: ['id', 'citation-label', 'number', 'URL'],
        convert: {
            toTarget (id) {
                const number = createCdliNumber(id)
                return [
                    number,
                    `CDLI:${number}`,
                    number,
                    `https://cdli.ucla.edu/${number}`
                ]
            }
        }
    },
    {
        source: 'languages',
        target: 'language',
        convert: {
            toTarget ([language]) {
                return language ? language.language : undefined
            }
        }
    }
])
