<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\ForbiddenException;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\Journal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Articles');
        $this->loadModel('ArticlesAuthors');
        $this->loadModel('Authors');

        $this->Auth->allow(['indexCdlj','indexCdln','indexCdlp','indexCdlb','viewCdln',
        'viewCdlp','viewCdlj','viewCdlb','viewCdlnWeb','viewCdljWeb','viewCdlbWeb']);
    }

    /**
     * indexCdln method
     */
    public function indexCdln()
    {
        $connection = ConnectionManager::get('default');
        
        $articles = $connection->execute("select * , articles.id as article_id,GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.article_type='cdln'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id; ")->fetchAll('assoc');

        $this->set(['cdln' => $articles ]);
    }

    /**
     * index_cdlb method
     */
    public function indexCdlb()
    {
        $connection = ConnectionManager::get('default');
        
        $articles = $connection->execute("select * , articles.id as article_id,GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.article_type='cdlb'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id; ")->fetchAll('assoc');

        $this->set(['cdlb' => $articles ]);
    }
    
    /**
     * indexCdlp method
     */
    public function indexCdlp()
    {
        $connection = ConnectionManager::get('default');
        
        $articles = $connection->execute("select * , articles.id as article_id,GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.article_type='cdlp'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id; ")->fetchAll('assoc');

        $this->set(['cdlp' => $articles ]);
    }

    /**
     * indexCdlj method
     */
    public function indexCdlj()
    {
        $connection = ConnectionManager::get('default');
        
        $articles = $connection->execute("select * , articles.id as article_id,GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.article_type='cdlj'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id; ")->fetchAll('assoc');

        $this->set(['cdlj' => $articles ]);
    }

    
    /**
     * viewCdln method
     */
    public function viewCdln()
    {
        $this->autoRender = false;

        $article = $this->request->params["cdln"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdln'])->first();

        if ($article) {
            $this->response->file(
                'webroot/pubs/cdln/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }

    /**
     * view_article_image method
     */
    public function viewArticleImage()
    {
        $this->autoRender = false;

        $article = $this->request->params["article"];
        $name = $this->request->params["name"];

        $this->response->file(
            '/srv/app/cake/webroot/pubs/'.$article.'/images'.'/'.$name
        );
    }

    /**
     * viewCdln method
     */
    public function viewCdlnWeb()
    {
        $this->autoRender = false;

        $article = $this->request->params["cdln"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdln'])->first();

        if ($article) {
            $this->layout= '';
            $this->set(['cdln' => $article ]);
            $this->render('cdln_web_template');
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }


    /**
     * viewCdln method
     */
    public function viewCdlbWeb()
    {
        $this->autoRender = false;

        $article = $this->request->params["cdlb"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlb'])->first();

        if ($article) {
            $this->layout= '';
            $this->set(['cdln' => $article ]);
            $this->render('cdln_web_template');
        } else {
            throw new ForbiddenException(__('No such CDLB article.'));
        }
    }

    /**
     * viewCdlj method
     */
    public function viewCdljWeb()
    {
        $this->autoRender = false;

        $article = $this->request->params["cdlj"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlj'])->first();

        if ($article) {
            $this->layout= '';
            $this->set(['cdln' => $article ]);
            $this->render('cdln_web_template');
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }

    /**
     * viewCdlp method
     */
    public function viewCdlp()
    {
        $this->autoRender = false;
        $article = $this->request->params["cdlp"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlp'])->first();
        if ($article) {
            $this->response->file(
                'webroot/pubs/cdlp/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLP article.'));
        }
    }
    /**
     * viewCdlb method
     */
    public function viewCdlb()
    {
        $this->autoRender = false;
        $article = $this->request->params["cdlb"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlb'])->first();
        if ($article) {
            $this->response->file(
                'webroot/pubs/cdlb/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLB article.'));
        }
    }

    /**
     * viewCdlp method
     */
    public function viewCdlj()
    {
        $this->autoRender = false;
        $article = $this->request->params["cdlj"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlj'])->first();
        if ($article) {
            $this->response->file(
                'webroot/pubs/cdlj/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLJ article.'));
        }
    }
}
