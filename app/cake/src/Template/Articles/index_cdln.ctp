<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 */
?>

<h3 class="display-4 pt-3"><?= __('Cuneiform Digital Library Notes') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th>No.</th>
            <th>Author</th>
            <th>Title</th>
            <th>Date</th>
            <th>View</th>
        </tr>
    </thead>
    <tbody align="left"  class="journals-view-table">
        <?php $cdln_ids = array();  ?>
        <?php $i=1; foreach ($cdln as $artcile) {  ?>
            <tr>
                <td width="5%"><?= $artcile['serial']; ?></td>
                <td width="25%"><?= $artcile['authors']; ?></td>
                <td width="50%"><?= $artcile['title']; ?></td>
                <td width="15%"><?= $artcile['created']; ?></td>
                <td width="15%"><a href="cdln/view/<?= $artcile['article_id']; ?>/web">View</a> </td>
            </tr>
       <?php } ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
    </ul>
</div>

